#include "root.h"
/**
 * _strlen - Returns length of string.
 * @s: Pointer to characters string.
 *
 * Return: Length of character string.
 */
int _strlen(const char *s)
{
    int length = 0;

    if (!s)
        return (length);
    for (length = 0; s[length]; length++)
        ;
    return (length);
}
/**
 * _strcpy - echo of string directs to src, includes terminating null
 * byte, to buffer directs by des.
 * @src: Point to src of source string.
 * @dest: Point to destination of echod string.
 *
 * Return: Pointer to the dest.
 */
char *_strcpy(char *dest, const char *src)
{
    size_t i;

    for (i = 0; src[i] != '\0'; i++)
        dest[i] = src[i];
    dest[i] = '\0';
    return (dest);
}
/**
 * _strcat - merge two strings.
 * @dest: Point to destination string.
 * @src: Point to source string.
 *
 * Return: Point to destination string.
 */
char *_strcat(char *dest, const char *src)
{
    char *destTemp;
    const char *srcTemp;

    destTemp = dest;
    srcTemp = src;
    while (*destTemp != '\0')
        destTemp++;
    while (*srcTemp != '\0')
        *destTemp++ = *srcTemp++;
    *destTemp = '\0';
    return (dest);
}
/**
 * _strncat - Merge two strings where n number of bytes are
 * echod from source.
 * @n: n bytes to echo from src.
 * @src: Point to source string.
 * @dest: Point to destination string.
 *
 * Return: Point to destination string.
 */
char *_strncat(char *dest, const char *src, size_t n)
{
    size_t dest_len = _strlen(dest);
    size_t i;

    for (i = 0; i < n && src[i] != '\0'; i++)
        dest[dest_len + i] = src[i];
    dest[dest_len + i] = '\0';
    return (dest);
}