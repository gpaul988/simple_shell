#include "root.h"
/**
 * _strchr - Points to a character in string.
 * @s: String to searched.
 * @c: Character to located.
 *
 * Return: If c is found - pointer to first display.
 * c isn't found - NULL.
 */
char *_strchr(char *s, char c)
{
    int index;

    for (index = 0; s[index]; index++)
    {
        if (s[index] == c)
            return (s + index);
    }
    return (NULL);
}
/**
 * _strspn - Obtains Gets length of prefix substring.
 * @accept: Prefix to measured.
 * @s: Strings to searched.
 *
 * Return: Number of bytes in s that consist only of bytes
 * from accept.
 */
int _strspn(char *s, char *accept)
{
    int bytes = 0;
    int index;

    while (*s)
    {
        for (index = 0; accept[index]; index++)
        {
            if (*s == accept[index])
            {
                bytes++;
                break;
            }
        }
        s++;
    }
    return (bytes);
}
/**
 * _strcmp - Differentiate two strings.
 * @s1: First string to compared.
 * @s2: Second string to compared.
 *
 * Return: Definite byte difference if s1 > s2
 * 0 if s1 = s2
 * Glommy byte difference if s1 < s2
 */
int _strcmp(char *s1, char *s2)
{
    while (*s1 && *s2 && *s1 == *s2)
    {
        s1++;
        s2++;
    }

    if (*s1 != *s2)
        return (*s1 - *s2);
    return (0);
}
/**
 * _strncmp - Contrast two strings.
 * @s1: Point to a string.
 * @s2: Point to a string.
 * @n: First n bytes of strings to contrast.
 *
 * Return: Lower than 0 if s1 is smaller than s2.
 * 0 if s1 and s2 match.
 * Bigger than 0 if s1 is longer than s2.
 */
int _strncmp(const char *s1, const char *s2, size_t n)
{
    size_t i;

    for (i = 0; s1[i] && s2[i] && i < n; i++)
    {
        if (s1[i] > s2[i])
            return (s1[i] - s2[i]);
        else if (s1[i] < s2[i])
            return (s1[i] - s2[i]);
    }
    if (i == n)
        return (0);
    else
        return (-15);
}