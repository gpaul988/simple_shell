#include "root.h"

/**
 * _copyenv - Develops copy of environment.
 *
 * Return: If error occurs - NULL.
 * Otherwise - Double pointer to new copy.
 */
char **_copyenv(void)
{
    char **new_environ;
    size_t size;
    int index;

    for (size = 0; environ[size]; size++)
        ;
    new_environ = malloc(sizeof(char *) * (size + 1));
    if (!new_environ)
        return (NULL);
    for (index = 0; environ[index]; index++)
    {
        new_environ[index] = malloc(_strlen(environ[index]) + 1);

        if (!new_environ[index])
        {
            for (index--; index >= 0; index--)
                free(new_environ[index]);
            free(new_environ);
            return (NULL);
        }
        _strcpy(new_environ[index], environ[index]);
    }
    new_environ[index] = NULL;
    return (new_environ);
}

/**
 * free_env - Frees an environment copy.
 */
void free_env(void)
{
    int index;

    for (index = 0; environ[index]; index++)
        free(environ[index]);
    free(environ);
}

/**
 * _getenv - Obtains environmental variable from PATH.
 * @var: Name of environmental variable to get.
 *
 * Return: If environmental variable don't exist - NULL.
 * O/W - Pointer to environmental variable.
 */
char **_getenv(const char *var)
{
    int index, len;

    len = _strlen(var);
    for (index = 0; environ[index]; index++)
    {
        if (_strncmp(var, environ[index], len) == 0)
            return (&environ[index]);
    }
    return (NULL);
}